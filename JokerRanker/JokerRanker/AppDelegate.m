//
//  AppDelegate.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "AppDelegate.h"
#import "mainNavigationViewController.h"
#import "mainTabBarViewController.h"
#import "AllViewController.h"
#import "MoreTableViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [UMSocialData setAppKey:@"576b5c5d67e58e2811003420"];//设置友盟appkey
    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:@"3404441013" secret:@"0ebbf420ef28d558e71039a095fa1073" RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    NSLog(@"%@",NSHomeDirectory());
    [NSThread sleepForTimeInterval:2.0];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    
    MainTabBarViewController *mainTabBarCon = [[MainTabBarViewController alloc] init];
    
    AllViewController *jokeViewCon = [[AllViewController alloc] init];
    jokeViewCon.title = @"幽默段子";
    jokeViewCon.itemType = 0;
    jokeViewCon.requestType = @"joke";
    jokeViewCon.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"段子" image:[UIImage imageNamed:@"list"] selectedImage:[UIImage imageNamed:@"list"]];
    
    AllViewController *picViewCon = [[AllViewController alloc] init];
    picViewCon.title = @"内涵趣图";
    picViewCon.itemType = 1;
    picViewCon.requestType = @"qutu";
    picViewCon.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"趣图" image:[UIImage imageNamed:@"tab_smile"] selectedImage:[UIImage imageNamed:@"tab_smile"]];
    
    AllViewController *styViewCon = [[AllViewController alloc] init];
    styViewCon.title = @"时尚物语";
    styViewCon.itemType = 2;
    styViewCon.requestType = @"mm";
    styViewCon.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"物语" image:[UIImage imageNamed:@"tab_talk"] selectedImage:[UIImage imageNamed:@"tab_talk"]];
    
    AllViewController *vidViewCon = [[AllViewController alloc] init];
    vidViewCon.title = @"热门视频";
    vidViewCon.itemType = 3;
    vidViewCon.requestType = @"video";
    vidViewCon.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"视频" image:[UIImage imageNamed:@"Icon-Video"] selectedImage:[UIImage imageNamed:@"Icon-Video"]];
    
    MoreTableViewController *moreTabViewCon = [[MoreTableViewController alloc] init];
    moreTabViewCon.title = @"更多";
    moreTabViewCon.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"更多" image:[UIImage imageNamed:@"bar_more"] selectedImage:[UIImage imageNamed:@"bar_more"]];
    
    mainTabBarCon.viewControllers = @[jokeViewCon,picViewCon,styViewCon,vidViewCon,moreTabViewCon];
    mainTabBarCon.tabBar.tintColor = [UIColor redColor];
    MainNavigationViewController *mainNavCon = [[MainNavigationViewController alloc] initWithRootViewController:mainTabBarCon];
    self.window.rootViewController = mainNavCon;
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
    }
    return result;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
