//
//  disclaimerViewController.m
//  JokerRanker
//
//  Created by Mac on 16/6/27.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "disclaimerViewController.h"

@interface disclaimerViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *SC;
@property (strong,nonatomic) UIView *textVi;
@end

@implementation disclaimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.textVi = [[UIView alloc]init];
    [self.SC addSubview:self.textVi];
    [self.textVi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.SC.mas_top);
        make.left.equalTo(self.SC.mas_left);
        make.bottom.equalTo(self.SC.mas_bottom);
        make.right.equalTo(self.SC.mas_right);
        make.width.equalTo(self.SC.mas_width);
    }];
    UILabel *titlelab = [[UILabel alloc] init];
    titlelab.text = @"免责声明\n        在使用搞笑排行榜前，请您务必仔细阅读并透彻理解本声明。您可以选择不使用搞笑排行榜，但如果您使用搞笑排行榜，您的使用行为将被视为对本声明全部内容的认可。搞笑排行榜中的视频内容均为搞笑排行榜以非人工检索方式、根据您键入或选择的关键字自动生成到第三方网页的链接，除搞笑排行榜注明之服务条款外，其他一切因使用搞笑排行榜而可能遭致的意外、疏忽、侵权及其造成的损失（包括因下载被搜索链接到的第三方网站内容而感染电脑病毒），搞笑排行榜对其概不负责，亦不承担任何法律责任。任何通过使用搞笑排行榜而搜索链接到的第三方网页均系他人制作或提供，您可能从该第三方网页上获得视频及享用服务，搞笑排行榜对其合法性概不负责，亦不承担任何法律责任。搞笑排行榜搜索结果根据您键入或选择的关键字自动搜索获得并生成，不代表搞笑排行榜赞成被搜索链接到的第三方网页上的内容或立场。您应该对使用搜索引擎的结果自行承担风险。搞笑排行榜不做任何形式的保证：不保证搜索结果满足您的要求，不保证搜索服务不中断，不保证搜索结果的安全性、正确性、及时性、合法性。因网络状况、通讯线路、第三方网站等任何原因而导致您不能正常使用搞笑排行榜，搞笑排行榜不承担任何法律责任。搞笑排行榜尊重并保护所有使用搞笑排行榜用户的个人隐私权，您注册的用户名、电子邮件地址等个人资料，非经您亲自许可或根据相关法律、法规的强制性规定，搞笑排行榜不会主动地泄露给第三方。搞笑排行榜提醒您：您在使用搜索引擎时输入的关键字将不被认为是您的个人隐私资料。任何网站如果不想被搞笑排行榜收录（即不被搜索到），应该及时向搞笑排行榜反映，或者在其网站页面中根据拒绝蜘蛛协议（Robots Exclusion Protocol）加注拒绝收录的标记，否则，搞笑排行榜将依照惯例视其为可收录网站。任何单位或个人认为通过搞笑排行榜搜索链接到的第三方网页内容可能涉嫌侵犯其信息网络传播权，应该及时向搞笑排行榜提出书面权利通知，并提供身份证明、权属证明及详细侵权情况证明。搞笑排行榜在收到上述法律文件后，将会依法尽快断开相关链接内容。";
//    titlelab.lineBreakMode = UILineBreakModeWordWrap;
    titlelab.numberOfLines = 0;
    [self.SC addSubview:titlelab];
    [titlelab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textVi.mas_top).offset(8);
        make.left.equalTo(self.textVi.mas_left).offset(8);
        make.bottom.equalTo(self.textVi.mas_bottom).offset(-8);
        make.right.equalTo(self.textVi.mas_right).offset(8);
    }];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
