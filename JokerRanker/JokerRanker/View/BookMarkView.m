//
//  bookMarkView.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/28.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "BookMarkView.h"

@implementation BookMarkView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)willMoveToSuperview:(UIView *)newSuperview {
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.bounds = CGRectMake(0, 0, 44, 24);
    [self.leftButton setTitle:@"清空" forState:UIControlStateNormal];
    [self addSubview:self.leftButton];
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.bounds = CGRectMake(0, 0, 44, 24);
    [self.rightButton setTitle:@"返回" forState:UIControlStateNormal];
    [self addSubview:self.rightButton];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.text = @"书签记录";
    [self addSubview:self.titleLabel];
    self.leftButton.center = CGPointMake(30, self.titleLabel.center.y);
    self.rightButton.center = CGPointMake(self.bounds.size.width - 30, self.titleLabel.center.y);
}

@end
