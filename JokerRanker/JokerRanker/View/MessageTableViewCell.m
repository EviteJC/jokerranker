//
//  MessageTableViewCell.m
//  
//
//  Created by Dayer－PC on 16/6/27.
//
//

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell

- (void)awakeFromNib {
    self.thumbButton.layer.borderWidth = .5;
    self.thumbButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.shareButton.layer.borderWidth = .5;
    self.shareButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.collectButton.layer.borderWidth = .5;
    self.collectButton.layer.borderColor = [UIColor grayColor].CGColor;
}

@end
