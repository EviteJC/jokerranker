//
//  MessageTableViewCell.h
//  
//
//  Created by Dayer－PC on 16/6/27.
//
//

#import <UIKit/UIKit.h>

@interface MessageTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *contentButton;
@property (strong, nonatomic) IBOutlet UIButton *thumbButton;
@property (strong, nonatomic) IBOutlet UIButton *collectButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *bookMarkButton;
@property (strong, nonatomic) IBOutlet UIButton *videoButton;

@end
