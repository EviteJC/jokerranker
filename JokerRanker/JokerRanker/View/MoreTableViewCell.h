//
//  MoreTableViewCell.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *rowImageView;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

@end
