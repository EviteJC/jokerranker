//
//  JokerModel.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/26.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JokerModel : NSObject

@property (nonatomic, assign) NSInteger jokerIndex;
@property (nonatomic, assign) NSInteger jokerID;
@property (nonatomic, copy) NSString *creatTime;
@property (nonatomic, copy) NSString *titleText;
@property (nonatomic, copy) NSString *thumbUPNum;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *webUrl;
@property (nonatomic, copy) NSString *MP4Url;
@property (nonatomic, copy) NSDictionary *contentImageInfoDic;
@property (nonatomic, assign) BOOL orBookMark;
@property (nonatomic, assign) BOOL orThumbUp;
@property (nonatomic, assign) BOOL orCollect;

@end
