//
//  RequestModel.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/26.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "RequestModel.h"

@interface RequestModel ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation RequestModel

#pragma mark - singleModel

static RequestModel *singleModel = nil;
+ (instancetype)defaultModel {
    @synchronized(self) {
        if (!singleModel) {
            singleModel = [[RequestModel alloc] init];
        }
    }
    return singleModel;
}

+ (id)allocWithZone:(struct _NSZone *)zone {
    @synchronized(self) {
        if (!singleModel) {
            singleModel = [super allocWithZone:zone];
            return singleModel;
        }
    }
    return nil;
}

- (AFHTTPSessionManager *)sessionManager {
    if (!_sessionManager) {
        _sessionManager = [AFHTTPSessionManager manager];
    }
    return _sessionManager;
}

#pragma mark - requestMethods

- (void)getJokerType:(NSString *)typeString loadJokerID:(NSInteger)jokerID withCompleteHandler:(void(^)(NSArray *jokerArr,NSString *rowsNum))completeHandler {
    NSString *requestStr = [NSString stringWithFormat:@"http://xiaoliao.vipappsina.com/index.php/Api386/index/pad/0/sw/1/cid/%@/p/1/markId/%ld/date", typeString, jokerID];
    [self.sessionManager GET:requestStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *mutableArray = [NSMutableArray array];
        for (int i = 0; i < [responseObject[@"rows"] count]; i++) {
            JokerModel *joker = [JokerModel new];
            joker.jokerIndex = i;
            joker.jokerID = [responseObject[@"rows"][i][@"id"] integerValue];
            joker.creatTime = responseObject[@"rows"][i][@"timeStr"];
            joker.titleText = responseObject[@"rows"][i][@"title"];
            joker.thumbUPNum = responseObject[@"rows"][i][@"zan_num"];
            joker.webUrl = responseObject[@"rows"][i][@"web_url"];
            joker.MP4Url = responseObject[@"rows"][i][@"mp4_url"];
            joker.contentImageInfoDic =@{
                                                       @"url":responseObject[@"rows"][i][@"pic"],
                                                
                                                       @"type":responseObject[@"rows"][i][@"pic_t"],
                                                       
                                                       @"height":responseObject[@"rows"][i][@"pic_h"],
                                                       
                                                       @"width":responseObject[@"rows"][i][@"pic_w"]};
            [mutableArray addObject:joker];
        }
        completeHandler(mutableArray,responseObject[@"totalRow"]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@---Error:%@",typeString,error);
    }];
}

- (void)downloadImage:(NSString *)imageUrlString withImageName:(void(^)())completeHandler {
    NSString *name = [[imageUrlString componentsSeparatedByString:@"/"] lastObject];
    if ([name rangeOfString:@"."].location == NSNotFound) {
//        name = [name stringByAppendingString:@".jpg"];
        return;
    }
    NSString *currentImagePath = [[self imagePath] stringByAppendingPathComponent:name];
    if (![[NSFileManager defaultManager] fileExistsAtPath:currentImagePath]) {
        [[self.sessionManager downloadTaskWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageUrlString]] progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
            return [NSURL fileURLWithPath:[[self imagePath] stringByAppendingPathComponent:name]];
        } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
            completeHandler();
            if (error) {
                NSLog(@"image download error:%@",error);
            }
        }] resume];
    } else {
        completeHandler();
    }
}

- (void)doThumbUpNewsID:(NSInteger)newsID withJokerType:(NSString *)typeString {
    [self.sessionManager POST:@"http://xiaoliao.vipappsina.com/index.php/Api386/doZan" parameters:@{@"news_id":[NSString stringWithFormat:@"%ld", newsID], @"type":typeString} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject:%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Thumb Up Error:%@",error);
    }];
}

#pragma mark - filePath

- (NSString *)imagePath {
    NSString *pathString = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Pictures"];
    [[NSFileManager defaultManager] createDirectoryAtPath:pathString withIntermediateDirectories:YES attributes:nil error:nil];
    return pathString;
}

@end
