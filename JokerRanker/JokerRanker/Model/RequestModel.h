//
//  RequestModel.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/26.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JokerModel.h"

@interface RequestModel : NSObject

+ (instancetype)defaultModel;

- (void)getJokerType:(NSString *)typeString loadJokerID:(NSInteger)jokerID withCompleteHandler:(void(^)(NSArray *jokerArr,NSString *rowsNum))completeHandler;

- (void)downloadImage:(NSString *)imageUrlString withImageName:(void(^)())completeHandler;

- (void)doThumbUpNewsID:(NSInteger)newsID withJokerType:(NSString *)typeString;

- (NSString *)imagePath;

@end
