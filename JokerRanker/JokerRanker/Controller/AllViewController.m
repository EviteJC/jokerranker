//
//  picViewController.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "AllViewController.h"
#import "RequestModel.h"
#import "MessageTableViewCell.h"
#import "BookMarkViewController.h"
#import "MainTabBarViewController.h"
#import "DetailViewController.h"
#import "PictureViewController.h"
#import "MoreTableViewController.h"

@interface AllViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *downLoadLabel;
@property (strong, nonatomic) IBOutlet UIView *downloadView;
@property (strong, nonatomic) IBOutlet UILabel *loadLabel;
@property (nonatomic, assign) NSInteger sumNumber;

@end

@implementation AllViewController

#pragma mark - lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    //    [self.tableView registerNib:[UINib nibWithNibName:@"MessageTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellID"];
    self.orUsePhotoType = ((MoreTableViewController *)self.tabBarController.viewControllers[4]).photoSwitch.on;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.sectionFooterHeight = 0;
    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    __weak typeof(self) weakSelf = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadDataFromID:0 withCompleteHandler:^(NSArray *jokerArr) {
            weakSelf.dataArray = jokerArr;
            for (int i = 0; i < ((MainTabBarViewController *)self.tabBarController).bookMarkArray.count; i++) {
                NSDictionary *dictionary = ((MainTabBarViewController *)self.tabBarController).bookMarkArray[i];
                for (JokerModel *joker in weakSelf.dataArray) {
                    if (joker.jokerID == ((JokerModel *)dictionary[@"joker"]).jokerID) {
                        joker.orBookMark = YES;
                    }
                }
            }
            [weakSelf.tableView reloadData];
            [weakSelf.downLoadLabel removeFromSuperview];
            [weakSelf.tableView.mj_header endRefreshing];
            weakSelf.loadLabel.text = [NSString stringWithFormat:@"   获取到最新16条，总共%ld条", self.sumNumber];
            [weakSelf downloadView];
            [UIView animateWithDuration:.5 animations:^{
                [weakSelf setDownloadViewHeight:40];
            } completion:^(BOOL finished) {
                [weakSelf performSelector:@selector(downloadViewDisplayAction) withObject:nil afterDelay:1.5];
            }];
        }];
    }];
    [header setTitle:@"松开立即刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新" forState:MJRefreshStateRefreshing];
    [header setTitle:@"刷新完成" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadDataFromID:((JokerModel *)[self.dataArray lastObject]).jokerID withCompleteHandler:^(NSArray *jokerArr) {
            NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:self.dataArray];
            [mutableArray removeLastObject];
            [mutableArray addObjectsFromArray:jokerArr];
            weakSelf.dataArray = mutableArray;
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_footer endRefreshing];
        }];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = self.title;
    self.tabBarController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shuqian"] style:UIBarButtonItemStylePlain target:self action:@selector(bookmarkLeft)];
    self.tabBarController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"rili"] style:UIBarButtonItemStylePlain target:self action:@selector(calendarRight)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"MessageTableViewCell" owner:nil options:nil][0];
    }
    [cell.contentButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[[RequestModel defaultModel] imagePath] stringByAppendingPathComponent:((JokerModel *)self.dataArray[indexPath.row]).imageName]] forState:UIControlStateNormal];
    [cell.contentButton addTarget:self action:@selector(goDetail:) forControlEvents:UIControlEventTouchUpInside];
    [cell.videoButton addTarget:self action:@selector(goDetail:) forControlEvents:UIControlEventTouchUpInside];
    cell.contentButton.tag = indexPath.row;
    cell.videoButton.tag = indexPath.row;
    if (!(self.itemType&&((JokerModel *)self.dataArray[indexPath.row]).jokerID)) {
        [cell.contentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(0));
        }];
    } else if (self.itemType==3) {
        [cell.contentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@140);
            make.height.equalTo(@100);
        }];
        [cell.videoButton setImage:[UIImage imageNamed:@"playVideo"] forState:UIControlStateNormal];
    } else {
        cell.contentButton.userInteractionEnabled = self.orUsePhotoType;
        float ratio = [((JokerModel *)self.dataArray[indexPath.row]).contentImageInfoDic[@"height"] floatValue]/[((JokerModel *)self.dataArray[indexPath.row]).contentImageInfoDic[@"width"] floatValue];
        [cell.contentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(cell.titleLabel.mas_width);
            make.height.equalTo(cell.contentButton.mas_width).multipliedBy(ratio);
        }];
    }
    cell.timeLabel.text = ((JokerModel *)self.dataArray[indexPath.row]).creatTime;
    cell.titleLabel.font = [UIFont systemFontOfSize:((MainTabBarViewController *)self.tabBarController).textFont];
    cell.titleLabel.text = ((JokerModel *)self.dataArray[indexPath.row]).titleText;
    if (!((JokerModel *)self.dataArray[indexPath.row]).jokerID) {
        cell.timeLabel.text = @"广告";
        cell.titleLabel.text = @"已屏蔽";
        for (UIView *view in cell.subviews) {
            view.userInteractionEnabled = NO;
        }
    }
    cell.bookMarkButton.tag = indexPath.row;
    cell.bookMarkButton.selected = ((JokerModel *)self.dataArray[indexPath.row]).orBookMark;
    [cell.bookMarkButton addTarget:self action:@selector(bookmarkAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.thumbButton.tag = indexPath.row;
    cell.thumbButton.selected = ((JokerModel *)self.dataArray[indexPath.row]).orThumbUp;
    [cell.thumbButton setTitle:((JokerModel *)self.dataArray[indexPath.row]).thumbUPNum forState:UIControlStateNormal];
    [cell.thumbButton addTarget:self action:@selector(doThumbUpAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.collectButton.tag = indexPath.row;
    cell.collectButton.selected = ((JokerModel *)self.dataArray[indexPath.row]).orCollect;
    [cell.collectButton addTarget:self action:@selector(collectAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - userMethods

- (UIView *)downloadView {
    [self setDownloadViewHeight:0];
    [self.view addSubview:_downloadView];
    return _downloadView;
}

- (void)loadDataFromID:(NSInteger)jokerID withCompleteHandler:(void(^)(NSArray *jokerArr))completeHandler{
    [[RequestModel defaultModel] getJokerType:self.requestType loadJokerID:jokerID withCompleteHandler:^(NSArray *jokerArr, NSString *rowsNum) {
        self.sumNumber = [rowsNum integerValue];
        completeHandler(jokerArr);
        for (JokerModel *joker in jokerArr) {
            if (![joker.contentImageInfoDic[@"url"] isEqualToString:@""]) {
                [[RequestModel defaultModel] downloadImage:joker.contentImageInfoDic[@"url"] withImageName:^(NSString *imageName) {
                    joker.imageName = [[joker.contentImageInfoDic[@"url"] componentsSeparatedByString:@"/"] lastObject];
                    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:joker.jokerIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }];
            }
        }
    }];
}

- (void)goDetail:(UIButton *)sender {
    if (self.itemType == 3) {
        DetailViewController *detailController = [[DetailViewController alloc] init];
        detailController.selectIndex = sender.tag;
        detailController.dataArr = self.dataArray;
        [self.navigationController pushViewController:detailController animated:YES];
    } else {
        PictureViewController *pictureController = [[PictureViewController alloc] init];
        pictureController.selectIndex = sender.tag;
        pictureController.dataArr = self.dataArray;
        [self.navigationController pushViewController:pictureController animated:YES];
    }
}

- (void)bookmarkLeft {
    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertCon addAction:cancel];
    UIAlertAction *refresh = [UIAlertAction actionWithTitle:@"继续阅读" style:UIAlertActionStyleDestructive handler:nil];
    [alertCon addAction:refresh];
    UIAlertAction *browse = [UIAlertAction actionWithTitle:@"查看书签" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        BookMarkViewController *bookMarkController = [[BookMarkViewController alloc] init];
        bookMarkController.bookMarkArray = ((MainTabBarViewController *)self.tabBarController).bookMarkArray;
        [self presentViewController:bookMarkController animated:YES completion:nil];
    }];
    [alertCon addAction:browse];
    [self presentViewController:alertCon animated:YES completion:nil];
}

- (void)calendarRight {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"功能尚未实现，敬请期待" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *enter = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:enter];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)bookmarkAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    ((JokerModel *)self.dataArray[sender.tag]).orBookMark = sender.selected;
    if (sender.selected) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [((MainTabBarViewController *)self.tabBarController).bookMarkArray insertObject:@{@"type":[NSNumber numberWithInteger:self.itemType],@"joker":self.dataArray[sender.tag],@"time":[dateFormatter stringFromDate:[NSDate date]]} atIndex:0];
    } else {
        for (int i = 0; i < ((MainTabBarViewController *)self.tabBarController).bookMarkArray.count; i++) {
            NSDictionary *dictionary = ((MainTabBarViewController *)self.tabBarController).bookMarkArray[i];
            for (JokerModel *joker in self.dataArray) {
                if (((JokerModel *)dictionary[@"joker"]).jokerID == joker.jokerID) {
                    [((MainTabBarViewController *)self.tabBarController).bookMarkArray removeObject:dictionary];
                    break;
                }
            }
        }
    }
}

- (void)doThumbUpAction:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    ((JokerModel *)self.dataArray[sender.tag]).orThumbUp = YES;
    [sender setTitle:[NSString stringWithFormat:@"%ld", [sender.currentTitle integerValue]+1] forState:UIControlStateSelected];
    sender.selected = YES;
    [[RequestModel defaultModel] doThumbUpNewsID:sender.tag withJokerType:self.requestType];
}

- (void)collectAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    ((JokerModel *)self.dataArray[sender.tag]).orCollect = sender.selected;
}

- (void)downloadViewDisplayAction {
    [UIView animateWithDuration:.5 animations:^{
        [self setDownloadViewHeight:0];
    } completion:^(BOOL finished) {
        [_downloadView removeFromSuperview];
    }];
}

- (void)setDownloadViewHeight:(CGFloat)height {
    _downloadView.frame = CGRectMake(0, 0, self.view.bounds.size.width, height);
    [_downloadView layoutSubviews];
//    for (UIView *vi in _downloadView.subviews) {
//        CGRect rect = vi.frame;
//        rect.size.height = height;
//        vi.frame = rect;
//    }
}

@end