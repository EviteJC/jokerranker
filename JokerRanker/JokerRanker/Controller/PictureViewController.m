//
//  PictureViewController.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/7/2.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "PictureViewController.h"
#import "RequestModel.h"

@interface PictureViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation PictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    UIView *view = [[UIView alloc] initWithFrame:self.navigationController.navigationBar.bounds];
//    view.backgroundColor = [UIColor blackColor];
//    [self.navigationController.navigationBar addSubview:view];
//    UIView *superview = self.navigationController.navigationBar;
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(superview.mas_top);
//        make.bottom.equalTo(superview.mas_bottom);
//        make.left.equalTo(superview.mas_left);
//        make.right.equalTo(superview.mas_right);
//    }];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.imageView.image = [UIImage imageWithContentsOfFile:[[[RequestModel defaultModel] imagePath] stringByAppendingPathComponent:((JokerModel *)self.dataArr[self.selectIndex]).imageName]];
    float ratio = [((JokerModel *)self.dataArr[self.selectIndex]).contentImageInfoDic[@"height"] floatValue]/[((JokerModel *)self.dataArr[self.selectIndex]).contentImageInfoDic[@"width"] floatValue];
    for (NSLayoutConstraint *constraint in  self.imageView.constraints) {
        constraint.active = NO;
    }
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.equalTo(self.view);
//        make.centerY.equalTo(self.view.mas_centerY);
//        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(self.imageView.mas_width).multipliedBy(ratio);
    }];
    self.titleLabel.text = ((JokerModel *)self.dataArr[self.selectIndex]).titleText;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
