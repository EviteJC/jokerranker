//
//  MoreTableViewController.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "MoreTableViewController.h"
#import "MoreTableViewCell.h"
#import "RequestModel.h"
#import "TextFontViewController.h"
#import "MainTabBarViewController.h"
#import "AllViewController.h"
#import "disclaimerViewController.h"
#import "loginViewController.h"

@interface MoreTableViewController () <UITableViewDataSource, UITableViewDelegate, UMSocialUIDelegate> {
    CGFloat _curLight;
}

@property (nonatomic, strong) NSArray *itemsArr;
@property (nonatomic, strong) UILabel *textFontLabel;
@property (nonatomic, strong) UILabel *imageDataLabel;
@property (strong, nonatomic) TextFontViewController *textFontController;

@end

@implementation MoreTableViewController

#pragma mark - lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.itemsArr = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"moreItem" ofType:@"plist"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.textFontString = [self judgeTextFont:((MainTabBarViewController *)self.tabBarController).textFont];
    self.tabBarController.navigationItem.title = self.title;
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_moon"] style:UIBarButtonItemStylePlain target:self action:@selector(chagBright)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 4;
            break;
        case 2:
            return 5;
            break;
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (!cell) {
        cell=[[NSBundle mainBundle] loadNibNamed:@"MoreTableViewCell" owner:nil options:nil][0];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.rowImageView.image = [UIImage imageNamed:self.itemsArr[indexPath.row+indexPath.section*(indexPath.section+1)][@"image"]];
    cell.contentLabel.text = self.itemsArr[indexPath.row+indexPath.section*(indexPath.section+1)][@"content"];
    if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 1:
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                self.textFontLabel.text = self.textFontString;
                [cell addSubview:self.textFontLabel];
                UIView *superview = self.textFontLabel.superview;
                [self.textFontLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(superview.mas_right).offset(-12);
                    make.centerY.equalTo(superview.mas_centerY);
                }];
            }
                break;
            case 2:
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                self.imageDataLabel.text = [NSString stringWithFormat:@"%.2fM", [self imageSizeAtPath:[[RequestModel defaultModel] imagePath]]];
                [cell addSubview:self.imageDataLabel];
                UIView *superview = self.imageDataLabel.superview;
                [self.imageDataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(superview.mas_right).offset(-12);
                    make.centerY.equalTo(superview.mas_centerY);
                }];
            }
                break;
            case 3:
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                [self.photoSwitch addTarget:self action:@selector(changePhotoType) forControlEvents:UIControlEventValueChanged];
                [cell addSubview:self.photoSwitch];
                UIView *superview = self.photoSwitch.superview;
                [self.photoSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(superview.mas_right).offset(-12);
                    make.centerY.equalTo(superview.mas_centerY);
                }];
            }
                break;
            default:
                break;
        }
    }
    return cell;
}

#pragma mark - tableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!section) {
        return 30;
    }
    return 18;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==2) {
        return 80;
    }
    return 18;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView cellForRowAtIndexPath:indexPath].selected = NO;
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    loginViewController *login = [[loginViewController alloc]init];
                    [self.tabBarController.navigationController pushViewController:login animated:YES];
                }
                    break;
                case 1:
                    
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    
                    break;
                case 1:
                {
                    [self.view addSubview:self.textChangeBackgroundView];
                    [UIView animateWithDuration:.5 animations:^{
                        self.textChangeBackgroundView.alpha = 0.3;
                    } completion:nil];
                    self.textFontController.modalPresentationStyle =UIModalPresentationOverCurrentContext;
                    self.textFontController.textString = self.textFontLabel.text;
                    [self presentViewController:self.textFontController animated:YES completion:nil];
                }
                    break;
                case 2:
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"清除缓存" message:@"清除缓存有利于节省空间,保留则可以浏览之前看过的图片更快，是否确定清除？" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:cancel];
                    UIAlertAction *enter = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[NSFileManager defaultManager] removeItemAtPath:[[RequestModel defaultModel] imagePath] error:nil];
                        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }];
                    [alert addAction:enter];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                    break;
                default:
                    break;
            }
            break;
        case 2:{
            switch (indexPath.row) {
                case 0:
                    {
                        [UMSocialData defaultData].extConfig.title = @"分享的title";
                        [UMSocialSnsService presentSnsIconSheetView:self
                                                             appKey:@"576b5c5d67e58e2811003420"
                                                          shareText:@"友盟社会化分享让您快速实现分享等社会化功能，http://umeng.com/social"
                                                         shareImage:nil
                                                    shareToSnsNames:@[UMShareToSina]
                                                           delegate:self];
                    }
                    break;
                case 4:
                {
                    disclaimerViewController *disclaimer = [[disclaimerViewController alloc]init];
                    [self.tabBarController.navigationController pushViewController:disclaimer animated:YES];
                }
                    break;
                    
                default:
                    break;
            }
        }
            
            break;
        default:
            break;
    }
}

#pragma mark - userMethods

- (NSString *)judgeTextFont:(NSInteger)textFont {
    NSString *textFontString;
    switch (textFont) {
        case 12:
            textFontString = @"最小";
            break;
        case 16:
            textFontString = @"较小";
            break;
        case 20:
            textFontString = @"中等";
            break;
        case 22:
            textFontString = @"较大";
            break;
        case 24:
            textFontString = @"最大";
            break;
        default:
            break;
    }
    return textFontString;
}

- (void)changePhotoType {
    for (int i = 1; i < 3; i ++) {
        AllViewController *allController = self.tabBarController.viewControllers[i];
        allController.orUsePhotoType = self.photoSwitch.on;
        [allController.tableView reloadData];
    }
}

- (TextFontViewController *)textFontController {
    if (!_textFontController) {
        _textFontController = [[TextFontViewController alloc] init];
    }
    return _textFontController;
}

- (UILabel *)textFontLabel {
    if (!_textFontLabel) {
        _textFontLabel = [[UILabel alloc] init];
        _textFontLabel.backgroundColor = [UIColor clearColor];
        _textFontLabel.textAlignment = NSTextAlignmentRight;
        _textFontLabel.textColor = [UIColor grayColor];
    }
    return _textFontLabel;
}

- (UILabel *)imageDataLabel {
    if (!_imageDataLabel) {
        _imageDataLabel = UILabel.new;
        _imageDataLabel.backgroundColor = [UIColor clearColor];
        _imageDataLabel.textAlignment = NSTextAlignmentRight;
        _imageDataLabel.textColor = [UIColor grayColor];
    }
    return _imageDataLabel;
}

- (UISwitch *)photoSwitch {
    if (!_photoSwitch) {
        _photoSwitch = [[UISwitch alloc] init];
        _photoSwitch.on = YES;
        _photoSwitch.onTintColor = [UIColor orangeColor];
    }
    return _photoSwitch;
}

- (void)chagBright {
    static BOOL orChg = YES;
    if (orChg) {
        _curLight = [UIScreen mainScreen].brightness;
        [[UIScreen mainScreen] setBrightness:0.5];
    } else {
        [[UIScreen mainScreen] setBrightness:_curLight];
    }
    orChg = !orChg;
}

- (void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}

- (float)imageSizeAtPath:(NSString *)imagePath {
    NSFileManager *manager = [NSFileManager defaultManager];
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:imagePath] objectEnumerator];
    NSString *fileName;
    NSInteger folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString *fileAbsolutePath = [imagePath stringByAppendingPathComponent:fileName];
        folderSize += [[manager attributesOfItemAtPath:fileAbsolutePath error:nil] fileSize];
    }
    return folderSize/(1024.0*1024.0);
}

@end
