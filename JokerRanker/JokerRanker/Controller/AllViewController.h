//
//  picViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger itemType;
@property (nonatomic, assign) BOOL orUsePhotoType;
@property (nonatomic, copy) NSString *requestType;
@property (nonatomic, copy) NSArray *dataArray;
@property (nonatomic, strong) NSMutableArray *mainDataArr;

@end
