//
//  TextFontViewController.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/29.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "TextFontViewController.h"
#import "MoreTableViewController.h"
#import "MainTabBarViewController.h"
#import "AllViewController.h"

@interface TextFontViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIPickerView *textFontPickView;
@property (nonatomic, copy) NSArray *textArray;
@property (nonatomic, copy) NSDictionary *textFontDictionary;

@end

@implementation TextFontViewController

#pragma mark - lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    for (NSString *textFontString in self.textArray) {
        if ([self.textString isEqualToString:textFontString]) {
            [self.textFontPickView selectRow:[self.textArray indexOfObject:textFontString] inComponent:0 animated:NO];
            return;
        }
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - pickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 5;
}

#pragma mark - pickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.textArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.textString = self.textArray[row];
}

#pragma mark - userMethods

- (NSArray *)textArray {
    if (!_textArray) {
        _textArray = @[@"最小",@"较小",@"中等",@"较大",@"最大"];
    }
    return _textArray;
}

- (NSDictionary *)textFontDictionary {
    if (!_textFontDictionary) {
        _textFontDictionary = @{@"最小":@12,@"较小":@16,@"中等":@20,@"较大":@24,@"最大":@28};
    }
    return _textFontDictionary;
}

- (IBAction)dissmissPickViewAction:(UIButton *)sender {
    MainTabBarViewController *mainTabbarController = (MainTabBarViewController *)self.presentingViewController;
    MoreTableViewController *moreTableController = mainTabbarController.viewControllers[4];
    [self dismissViewControllerAnimated:YES completion:nil];
    [UIView animateWithDuration:.5 animations:^{
        moreTableController.textChangeBackgroundView.alpha = 0;
    } completion:^(BOOL finished) {
        [moreTableController.textChangeBackgroundView removeFromSuperview];
        mainTabbarController.textFont = [self.textFontDictionary[self.textString] integerValue];
        moreTableController.textFontString = self.textString;
        [moreTableController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
        for (int i = 0; i < 4; i ++) {
            AllViewController *allController = mainTabbarController.viewControllers[i];
            [allController.tableView reloadData];
        }
    }];
}

@end
