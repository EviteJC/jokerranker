//
//  DetailViewController.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/7/1.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "DetailViewController.h"
#import "JokerModel.h"

@interface DetailViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *thumbButton;
@property (strong, nonatomic) IBOutlet UIButton *collectButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = ((JokerModel *)self.dataArr[self.selectIndex]).titleText;
    [self.thumbButton setTitle:((JokerModel *)self.dataArr[self.selectIndex]).thumbUPNum forState:UIControlStateNormal];
    if ([((JokerModel *)self.dataArr[self.selectIndex]).MP4Url isEqualToString:@"web"]) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:((JokerModel *)self.dataArr[self.selectIndex]).webUrl]]];
    } else {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:((JokerModel *)self.dataArr[self.selectIndex]).MP4Url]]];
    }
    self.webView.scalesPageToFit = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
