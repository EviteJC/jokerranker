//
//  mainTabBarViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabBarViewController : UITabBarController

@property (nonatomic, strong) NSMutableArray *bookMarkArray;
@property (nonatomic, assign) NSInteger textFont;

@end
