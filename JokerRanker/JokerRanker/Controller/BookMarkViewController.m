//
//  BookMarkViewController.m
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/28.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import "BookMarkViewController.h"
#import "BookMarkView.h"
#import "BookMarkTableViewCell.h"
#import "JokerModel.h"
#import "AllViewController.h"
#import "RequestModel.h"

@interface BookMarkViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet BookMarkView *bookMarkView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BookMarkViewController

#pragma mark - lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.bookMarkView.leftButton addTarget:self action:@selector(cancelAllBookMarkAction) forControlEvents:UIControlEventTouchUpInside];
    [self.bookMarkView.rightButton addTarget:self action:@selector(goBackAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.estimatedRowHeight = 66;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerNib:[UINib nibWithNibName:@"BookMarkTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"cellID"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.bookMarkArray||!self.bookMarkArray.count) {
        return 1;
    }
    return self.bookMarkArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.bookMarkArray||!self.bookMarkArray.count) {        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.textLabel.text = @"暂无书签记录...";
        return cell;
    }
    BookMarkTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    cell.textLabel.text = ((JokerModel *)self.bookMarkArray[indexPath.row][@"joker"]).titleText;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"添加时间：%@", self.bookMarkArray[indexPath.row][@"time"]];
    return cell;
}

#pragma mark - tableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITabBarController *tabBarController = ((UITabBarController *)((UINavigationController *)self.presentingViewController).viewControllers[0]);
    tabBarController.selectedIndex = [self.bookMarkArray[indexPath.row][@"type"] integerValue];
    AllViewController *allController = (AllViewController *)tabBarController.viewControllers[[self.bookMarkArray[indexPath.row][@"type"] integerValue]];
    [[RequestModel defaultModel] getJokerType:allController.requestType loadJokerID:((JokerModel *)self.bookMarkArray[indexPath.row][@"joker"]).jokerID withCompleteHandler:^(NSArray *jokerArr, NSString *rowsNum) {
        for (JokerModel *joker in jokerArr) {
            if (![joker.contentImageInfoDic[@"url"] isEqualToString:@""]) {
                [[RequestModel defaultModel] downloadImage:joker.contentImageInfoDic[@"url"] withImageName:^(NSString *imageName) {
                    joker.imageName = [[joker.contentImageInfoDic[@"url"] componentsSeparatedByString:@"/"] lastObject];
                }];
            }
            for (NSDictionary *dictionary in self.bookMarkArray) {
                if (joker.jokerID == ((JokerModel *)dictionary[@"joker"]).jokerID) {
                    joker.orBookMark = YES;
                }
            }
        }
        allController.dataArray = jokerArr;
        [self dismissViewControllerAnimated:YES completion:nil];
        [allController.tableView reloadData];
        [allController.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }];
}

#pragma mark - userMethods

- (void)cancelAllBookMarkAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确定清空书签记录？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    UIAlertAction *enter = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        for (NSDictionary *dictionary in self.bookMarkArray) {
            ((JokerModel *)dictionary[@"joker"]).orBookMark = NO;
        }
        [self.bookMarkArray removeAllObjects];
        [self.tableView reloadData];
    }];
    [alert addAction:enter];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)goBackAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
