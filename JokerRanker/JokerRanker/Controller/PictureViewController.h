//
//  PictureViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/7/2.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureViewController : UIViewController

@property (nonatomic, copy) NSArray *dataArr;
@property (nonatomic, assign) NSInteger selectIndex;

@end
