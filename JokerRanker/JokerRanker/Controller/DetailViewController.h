//
//  DetailViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/7/1.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic, copy) NSArray *dataArr;
@property (nonatomic, assign) NSInteger selectIndex;

@end
