//
//  BookMarkViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/28.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookMarkViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *bookMarkArray;

@end
