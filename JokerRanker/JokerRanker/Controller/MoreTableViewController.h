//
//  MoreTableViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/24.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UIView *textChangeBackgroundView;
@property (nonatomic, strong) UISwitch *photoSwitch;
@property (nonatomic, copy) NSString *textFontString;

@end
