//
//  TextFontViewController.h
//  JokerRanker
//
//  Created by Dayer－PC on 16/6/29.
//  Copyright © 2016年 Dayer－PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFontViewController : UIViewController

@property (nonatomic, copy) NSString *textString;

@end
